import { FC, JSXElementConstructor, ReactElement, ReactFragment, ReactPortal, useState } from "react";
import LiveSearch from "./LiveSearch";
import './App.css'
import React from "react";

let locationString: any = []
interface Props { }
let uri = "/api/v1/location?locationString="
let uri1 = "/api/v1/location"


const App: FC<Props> = (props): JSX.Element => {




  const updatelocationString = async (location: { id: string, name: string }) => {

    const filteredValue1 = await fetch(uri1, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ name: location.name })
    }).then((response: any) => response)

    setSelectedProfile(location);
    setValue(value + 1)
    locationString.push(location)

  }
  const componentDidMount: any = () => {
    document.addEventListener("mousedown", handleClickOutside);
  }
  const componentWillUnmount: any = () => {
    document.removeEventListener("mousedown", handleClickOutside);
  }

  const handleClickOutside = (event: { target: any; }) => {
    if (container.current && !container.current.contains(event.target)) {
      setState({
        open: false,
      });
    }
  };

  type changeHandler = React.ChangeEventHandler<HTMLInputElement>;

  const handleChange: changeHandler = async (e) => {
    const { target } = e;
    let filteredValue = []
    if (!target.value.trim()) return setResults([]);

    const filteredValue1 = await fetch(uri + target.value, {
      method: "GET",
      headers: { "Content-Type": "application/json" }
    }).then((response: any) => response.json());

    for (let i = 0; i <= filteredValue1.data.length - 1; i++) {
      if (filteredValue1.data[i].locality != 'undefined') filteredValue.push({ id: String(i), name: filteredValue1.data[i].locality })
    }
    setResults(filteredValue);
  };


  const handleButtonClick = () => { setState((state: any) => { return { open: !state.open } }) };

  let [state, setState] = useState<{ open: boolean }>({ open: false });
  const [results, setResults] = useState<{ id: string; name: string }[]>();
  const [selectedProfile, setSelectedProfile] = useState<{ id: string; name: string; }>();
  const [value, setValue] = useState(0);
  const container = React.createRef<HTMLDivElement>();

  return (
    <div className="App">
      <LiveSearch
        results={results}

        renderItem={(item) => <p>{item.name}</p>}
        onChange={handleChange}
        onSelect={(item) => updatelocationString(item)}
      />
      <div className="serarchitems">
        {Array.from(Array(value), (e: any, i: any) => {
          if (i < 2) { return <span className='selectedvaluesdropdown'>{locationString[i].name}</span> } else if (i == 3) {
            return <div className="container" ref={container}> <button className="see-more-button" onClick={handleButtonClick}>See more...</button> {state.open && (<div className="dropdown">
              <ul>

                <li>{locationString[i].name}</li>

              </ul>
            </div>)}</div>;
          }


        })}

      </div></div>

  );
};

export default App;
